import { Outlet } from "react-router-dom";

import css from "./Layout.module.scss";

const Layout = () => {
  return (
    <section className={css.layout}>
      <Outlet />
    </section>
  );
};

export default Layout;
