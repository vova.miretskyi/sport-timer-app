import { Route, Routes } from "react-router-dom";

import PrivateRoutes from "./PrivateRoutes";

import { Test } from "pages";
import { Layout } from "components";

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/test" element={<Test />} />

      <Route
      // element={<ProtectedRoute isAuth={isAuth} />}
      >
        <Route path="/" element={<Layout />}>
          {PrivateRoutes()}
        </Route>
      </Route>
    </Routes>
  );
};

export default AppRoutes;
