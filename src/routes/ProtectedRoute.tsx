import { Navigate, Outlet } from "react-router-dom";

type Props = {
  isAuth: boolean;
  redirectPath?: string;
};

const ProtectedRoute = ({ isAuth, redirectPath = "/" }: Props) => {
  if (!isAuth) {
    return <Navigate to={redirectPath} replace />;
  }

  return <Outlet />;
};

export default ProtectedRoute;
