import { Route } from "react-router-dom";

import { Main } from "pages";
import { MAIN_PRIVATE_ROUTES } from "constants/routes";

const PrivateRoutes = () => (
  <>
    <Route path={MAIN_PRIVATE_ROUTES.MAIN} element={<Main />} />
  </>
);

export default PrivateRoutes;
